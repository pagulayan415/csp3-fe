import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Container } from 'react-bootstrap';
import Slider from '../components/Slider';
import Brands from './../components/Brands';
import Footer from '../components/Footer';

 
 
const Home = () => {
   const pageData = {
       title: "JM's SHOPPE",
       content: "Products for everyone, everywhere",
       destination: "/products",
       label: "Browse Products"
   };
 
   return(
       <React.Fragment>
           <Container fluid>
            <div className='banner-slider'>
            <Slider />
            <Banner data={pageData}/>
           </div>
           </Container>
           <Container fluid> 
               <div className='brandSlicks'>
                   <Brands />
               </div>
           </Container>
           <Container fluid className='featuredBody'>
               <div className='featuredContent'>
               <Highlights/>
               </div>
           </Container>
           <Footer />
       </React.Fragment>
   );
}
export default Home;