import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";


export default class AutoPlay extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      speed: 5000,
      autoplaySpeed: 100,
      cssEase: "linear"
    };
    return (
      <div>
        {/* <h2>Auto Play</h2> */}
        <Slider {...settings}>
          <div>
            <img src={require('./../assets/brands/1.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/2.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/3.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/4.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/5.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/6.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/7.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/8.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/9.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/11.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/12.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/15.png')}/>
          </div>
          <div>
          <img src={require('./../assets/brands/16.png')}/>
          </div>

        </Slider>
      </div>
    );
  }
}