import React, { useContext, useEffect, useState } from 'react';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

const AppNavBar = () => {

	const [email, setEmail] = useState(null)

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				// 'Content-Type': 'application/json',
				// 'Accept': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			// if (typeof data.email !== 'undefined'){
			// 	setEmail(data.email)
			//   } else {
			// 	setEmail(undefined)
			//   }
			// console.log(data.email)
			setEmail(data.email)
			
		})
	})

	const {user} = useContext(UserContext)
	return (
		
		<Navbar expand="lg" variant='dark' className='appNavBar'>
			<Container fluid>
					<Link className="navbar-brand navText" to="/">JM's SHOPPE</Link>
					<Navbar.Toggle aria-controls="basic-navbar-nav" className='navToggle'/>
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="ml-auto">
							<Link className="nav-link" to="/products">
								{user.isAdmin === true ?
										<span>Admin Dashboard</span>
									:
										<span>Products</span>
								}  
							</Link>
						</Nav>
						
						<Nav className="ms-auto">
						
							{user.id !== null ?
									user.isAdmin === true ?
											<NavDropdown title={email} id="basic-nav-dropdown">
												<Link className="nav-link" to="/logout">
													Log Out
												</Link>
											</NavDropdown>
										:
											<React.Fragment>
												<Link className="nav-link" to="">
												</Link>
												<NavDropdown title={email} id="basic-nav-dropdown">
												<Link className="nav-link" to="/cart">
													Cart
												</Link>
												<Link className="nav-link" to="/orders">
													Orders
												</Link>
												<Link className="nav-link" to="/logout">
													Log Out
												</Link>
												</NavDropdown>
												{/* <Link className="nav-link" to="/cart">
													Cart
												</Link>
												<Link className="nav-link" to="/orders">
													Orders
												</Link>
												<Link className="nav-link" to="/logout">
													Log Out
													{email}
												</Link> */}
											</React.Fragment>
								:
									<React.Fragment>
										<Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>
											Log In
										</Link>
										<Link className="nav-link" to="/register">
											Register
										</Link>
									</React.Fragment>
							}              
						</Nav>
					</Navbar.Collapse>
					</Container>
				</Navbar>

		   );

}

export default AppNavBar;